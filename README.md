# Kibako Integration-Tests

Project for automated browser-tests to test Kibako.

## Build
Run:
```sh
yarn install
```

## Run
First make sure that besides this directory, there are two directories called `kibako`,`oot_clean` and `mm_clean`.
All are used for testing.<br>
`oot_clean` / `mm_clean`  must be clean copies of each game that a correct checksum.


<br>

### Scene Tests

This test is used to verify that reading and writing produces the exact same reuslts.

To start the test run:
```sh
# for OOT:
node scene_test.js oot
# for MM:
node scene_test.js mm
```

This will go through all scenes and saves them.<br>
After the test is done, oot/mm will be build and checksum is used to verify the result.