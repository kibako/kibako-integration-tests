const { spawn } = require('node:child_process');

function spawnProc(cmd, args, cwd, stdOutCb, codeCb)
{
  const proc = spawn(cmd, args, {cwd, shell: true});

  proc.stdout.on('data', (data) => 
  { 
    const dataStr = `${data}`;
    if(stdOutCb)stdOutCb(dataStr);
    console.log(`${data}`);
  });

  proc.stderr.on('data', (data) => { console.error(`stderr: ${data}`); });

  proc.on('close', (code) => { 
    console.log(`Process exited with: ${code}`); 
    if(codeCb)codeCb(code);
  }); 

  return proc;
}

module.exports = {spawnProc};
