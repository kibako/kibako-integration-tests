const { spawnProc } = require("./proc.js");
const path = require("path");
const {Builder, By, Key, until} = require('selenium-webdriver');
const { writeFileSync } = require("fs");
const homedir = require('os').homedir();

const GAME = process.argv[process.argv.length-1] === "mm" ? "mm" : "oot";

const KIBAKO_DIR = path.resolve("..", "kibako");

const OOT_DIR = path.resolve("..", "oot_clean");
const MM_DIR = path.resolve("..", "mm_clean");

const SOURCE_DIR = {mm: MM_DIR, oot: OOT_DIR}[GAME];
const SCENE_COUNT = {mm: 0x65, oot: 0x6D}[GAME];

let targetUrl = "";

async function sleep(ms) {
  return new Promise(resolve => {setTimeout(() => resolve(), ms);});
}

function spawnPromise(cmd, args) {
  return new Promise((resolve, reject) => {
   spawnProc(cmd, args, SOURCE_DIR, null, code => {
     if(code != 0)return reject(code);
     resolve(code);
   });
 });
 }

async function resetSources() {
  await spawnPromise("make", ["assetclean"]);
  await spawnPromise("make", ["-j", "setup"]);
  if(GAME === "mm") {
    await spawnPromise("make", ["-j", "init"]);
  }
  await spawnPromise("make", ["-j"]);
}

function buildFromSource() {
  return spawnPromise("make", ["-j"]);
}

// Patch config to open correct project
const configJson = JSON.stringify({
  "decompDir": SOURCE_DIR+"/",
  "lastScene": 0
});
writeFileSync(homedir + "/.config/kibako_config.json", configJson);

(async function main() {

  // start with a fresh copy
  await resetSources();

  // Start the native Kibako app...
  const procKibako = spawnProc(
    './app/cmake-build-release/kibako_native', ['--url=http://127.0.0.1:3000', '--test-mode'],
    KIBAKO_DIR,
    out => {
      // ...and wait for then URL to be logged
      if(out.includes(" URL: ")) {
        targetUrl = out.substring(out.indexOf(" URL: ") + 6);
      }
    }
  );

  // Wait until kibako is ready...
  while(targetUrl.length === 0) {
    await sleep(100);
  }

  let driver = await new Builder().forBrowser('chrome').build();
  await driver.manage().window().setRect({x: 1920*2 + 100, y: 100, width: 1440, height: 810});

  try {
    // Wait for page to load
    await driver.get(targetUrl);
    await driver.wait(until.titleIs('Kibako'), 1000);
    await sleep(500);

    // Open the last project
    const loadLastElem = await driver.findElement(By.id("menu_open_last"));
    const actions = driver.actions({async: true});
    await actions.move({origin: loadLastElem}).click().perform();

    // Wait for scene to load
    await sleep(1000);

    // Find all scene entries in the list on the left
    const sceneListEntries = await driver.findElements(By.className("menu_list_scene_entry"));
    if(sceneListEntries.length != SCENE_COUNT+1) {
      console.error(sceneListEntries);
      throw new Error("Wrong Scene Count!");
    }

    // Now go through all scenes...
    for(const sceneEntryElem of sceneListEntries)
    {
      // click on entry on the left
      const actions = driver.actions({async: true});
      await actions.move({origin: sceneEntryElem}).click().perform();

      // wait until loaded
      await sleep(750);

      // click through tabs in the right,
      // this will force loading all the data i can edit in the UI
      const tabEntries = await (await driver.findElement(By.className("tab_selector"))).findElements(By.xpath("*"));
      if(tabEntries.length != 4) {
        throw new Error("Wrong Tab Count!");
      }

      for(const [i, tabEntry] of tabEntries.entries()) {
        const tabAction = driver.actions({async: true});
        await tabAction.move({origin: tabEntry}).click().perform();

        if(i < (tabEntries.length-1))await sleep(100);
      }

      // hit save
      await driver.actions().keyDown(Key.CONTROL).sendKeys('s').perform();
      await sleep(200);

      // check if checksum still matches
      await buildFromSource();

      // hit save again, some things read from source before saving (e.g. paths)
      await driver.actions().keyDown(Key.CONTROL).sendKeys('s').perform();
      await sleep(200);

      // check again if checksum still matches
      await buildFromSource();
    }

    // one final check
    await buildFromSource();

    console.log("Test Successful!");

  } catch(e) {
    console.error("Test failed!", e);
    await sleep(1000 * 2);
  }finally {
    await driver.quit();
    procKibako.kill();
  }
  
})();