set -e

echo "Testing OOT..."
node scene_test.js oot

sleep 1

echo "Testing MM..."
node scene_test.js mm

echo "Done"
